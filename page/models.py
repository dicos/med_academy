from datetime import date

from django.db import models
from django.core.validators import RegexValidator
from django.utils import timezone

from tinymce.models import HTMLField

from solo.models import SingletonModel


class RequestCall(models.Model):
    """ Заказать звонок """
    contact_name = models.CharField(max_length=100, verbose_name='ФИО')
    phone = models.CharField(max_length=18, verbose_name='контактный телефон', blank=True, null=True)
    email = models.EmailField(verbose_name="Email", blank=True, null=True, max_length=128)
    date_created = models.DateTimeField(verbose_name='Дата добавления', default=timezone.now)

    def __str__(self):
        text = (getattr(self, s) for s in ('contact_name', 'phone', 'email') if s)
        return " ".join(text)

    class Meta:
        verbose_name = 'Заказ звонка'
        verbose_name_plural = 'Заказы звонков'


class Education(models.Model):
    """ Уровень образования """
    name = models.CharField("Уровень образования", max_length=150)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Уровень образования"
        verbose_name_plural = 'Уровни образования'


class Entrant(models.Model):
    """ Заявки на поступление в академию """
    last_name = models.CharField("Фамилия", max_length=100, blank=True, null=True)
    first_name = models.CharField("Имя", max_length=100, blank=True, null=True)
    patronymic = models.CharField("Отчество", max_length=100, blank=True, null=True)
    _phone_validator = RegexValidator(regex=r'^\+7 \(\d{3}\) \d{3}\ \d{2}-\d{2}$',
                                      message='Номер телефона должен иметь такой формат: +7 (999) 999 99-99')
    phone = models.CharField("Номер телефона*", validators=[_phone_validator], max_length=18, blank=True, null=True)
    email = models.EmailField(max_length=128, verbose_name='Ваш e-mail', blank=True, null=True)
    city = models.CharField("Город", max_length=100, blank=True, null=True)
    education = models.ForeignKey(Education, verbose_name="Уровень образования", blank=True, null=True)
    comment = models.TextField("Комментарий", blank=True, null=True)
    date_created = models.DateTimeField(verbose_name='Дата добавления', default=timezone.now)

    def __str__(self):
        return " ".join((self.last_name, self.first_name, self.patronymic, self.date_created.strftime("%d.%m.%Y %H:%M:%S")))

    class Meta:
        verbose_name = "Заявка на поступление в академию"
        verbose_name_plural = "Заявки на поступление в академию"


class SiteText(SingletonModel):
    """ Текст на сайте """
    title = models.TextField("Title", max_length=100, default="Заголовок сайта мед. академии")
    keywords = models.CharField("Keywords", max_length=100, default="Ключевые слова мед. академии")
    description = models.TextField('Description', default="Описание сайта мед. академии")
    text1_1 = models.CharField("Блок №1(1)", max_length=100, default="8 800")
    text1_2 = models.CharField("Блок №1(2)", max_length=100, default="250 11 40")
    text2 = models.TextField('Блок №2', default="Спешим Вам сообщить")
    text3 = models.TextField('Блок №3', default="Дистанционная Медицинская Академия")
    text4 = models.TextField('Блок №4', default="откроется в январе 2017 года!")
    text4_1 = models.TextField('Блок №4 (текст)', default="Вы можете пройти обучение на сертификационных циклах по "
                                                          "фармацевтическим специальностям дистанционно!")
    text5 = models.TextField('Блок №5', default="Оставьте заявку")
    text6 = models.TextField('Блок №6', default="для поступления в нашу академию")
    entrant_good = models.TextField('Оставили заявку на поступление', default="Заявка принята, в ближайшее время с вами свяжутся")
    text7 = models.TextField('Блок №7', default="Дистанционная медицинская")
    text8 = models.TextField('Блок №8', default="академия это:")
    text9 = models.TextField('Блок №9', default="экономия средств")
    text10 = models.TextField('Блок №10', default="обучение в любое удобное время")
    text11 = models.TextField('Блок №11', default="обратная связь от квалифицированных преподавателей")
    text12 = models.TextField('Блок №12', default="законное получение удостоверения о повышении квалификации"
                                                  " и сертификата государственного образца сроком на 5 лет")
    text13 = models.TextField('Блок №13', default="обучение посредством современных технологий"
                                                  " и из любого уголка России")
    text14 = models.TextField('Блок №14', default="ДО ОТКРЫТИЯ ОСТАЛОСЬ")
    date_left = models.DateField(u"Дней осталось", default=date(2017, 1, 1))
    text15 = models.TextField('Блок №15',
                              default='Вы можете связаться с нами <br>по бесплатному телефону 88002501140'
                                      'или по <a href="mailto:expert@pharma-school.ru">электронной почте</a>')
    text16 = models.TextField('Блок №16', default="Наши преимущества:")
    text17 = models.TextField('Блок №17', default="Гибкий график обучения")
    text18 = models.TextField('Блок №18', default="Экономия от 10 000 рублей")
    text19 = models.TextField('Блок №19', default="Сертификат государственного образца")
    text20 = models.TextField('Блок №20', default="Как Вы можете дистанционно обучаться в нашей академии")
    step1_title = models.TextField("Шаг 1 заголовок", default="Заявка в нашу академию")
    step1 = models.TextField("Шаг 1 текст", default="После  получения  Вашей  заявки  мы  высылаем Вам"
                                                    " список  документов - досье фармацевтического специалиста")
    step2_title = models.TextField("Шаг 2 заголовок", default="Подписание договора и оплата услуг")
    step2 = models.TextField("Шаг 2 текст", default="Необходимо подписать договор, отправить его"
                                                    " нам по почте, затем - оплатить обучение.")
    step3_title = models.TextField("Шаг 3 заголовок", default="Вход в систему дистанционного обучения")
    step3 = models.TextField("Шаг 3 текст", default="После получения оплаты, мы предоставляем Вам уникальные "
                                                    "логин и пароль для дистанционного обучения на нашей площадке.")
    step4_title = models.TextField("Шаг 4 заголовок", default="Дистанционное обучение")
    step4 = models.TextField("Шаг 4 текст", default="Обучение посредством видеолекций, получение обратной связи от "
                                                    "преподавателей курсов, прохождение итогового тестирования.")
    step5_title = models.TextField("Шаг 5 заголовок", default="Итоги дистанционного обучения")
    step5 = models.TextField("Шаг 5 текст", default="После успешного завершения обучения Вы получаете по почте "
                                                    "документы о повышении квалификации установленного образца.")
    text21 = models.TextField("Блок 21", default='ВАЖНАЯ СПРАВОЧНАЯ ИНФОРМАЦИЯ:')
    text22 = models.TextField("Блок 22", default='Организации, осуществляющие образовательную деятельность, '
                                                 '(далее - организации) реализуют образовательные программы или их '
                                                 'части с применением электронного обучения, дистанционных '
                                                 'образовательных технологий в предусмотренных Федеральным законом от '
                                                 '29 декабря 2012 г. N 273-ФЗ "Об образовании в Российской Федерации" '
                                                 '1 формах получения образования и формах обучения или при их '
                                                 'сочетании, при проведении учебных занятий, практик, текущего '
                                                 'контроля успеваемости, промежуточной, итоговой и (или) '
                                                 'государственной итоговой аттестации обучающихся*.')
    text23 = models.TextField("Блок 23", default='* Приказ Министерства образования и науки Российской Федерации '
                                                 '(Минобрнауки России) от 9 января 2014 г. N 2 г. Москва "Об '
                                                 'утверждении Порядка применения организациями, осуществляющими '
                                                 'образовательную деятельность, электронного обучения, дистанционных '
                                                 'образовательных технологий при реализации образовательных программ"')
    text24 = models.TextField("Блок 24", default='Остались вопросы?')
    text25 = models.TextField("Блок 25", default='оставьте свои контактные данные, мы обязательно свяжемся с вами')
    callback_good = models.TextField('Оставили заявку на обратный звонок',
                                    default="Заявка принята, в ближайшее время с вами свяжутся")
    text26 = models.TextField("Блок 26", default='2016. Все права защищены')

    def get_days_left(self):
        """ До открытия осталось """
        td = self.date_left - timezone.now().date()
        return td.days


class Consent(SingletonModel):
    """ Согласие на обработку персональных данных """
    title = models.TextField("Title", max_length=100, default="Заголовок страницы соглашения")
    keywords = models.CharField("Keywords", max_length=100, default="Ключевые страницы соглашения")
    description = models.TextField('Description', default="Описание страницы соглашения")
    text = HTMLField(default="Текст страницы соглашения")

    class Meta:
        verbose_name = verbose_name_plural = u'Страница "Обработка персональных данных"'


class Lic(SingletonModel):
    """ Наша лицензия """
    title = models.TextField("Title", max_length=100, default="Заголовок лицензии")
    keywords = models.CharField("Keywords", max_length=100, default="Страницы лицензии")
    description = models.TextField('Description', default="Описание страницы лицензии")
    text = HTMLField(default="Текст страницы лицензии")

    class Meta:
        verbose_name = verbose_name_plural = u'Страница "Лицензия"'


class Price(SingletonModel):
    """ Прайс """
    title = models.TextField("Title", max_length=100, default="Заголовок прайса")
    keywords = models.CharField("Keywords", max_length=100, default="Страница парйса")
    description = models.TextField('Description', default="Описание страницы парйса")
    text = HTMLField(default="Текст страницы прайса")

    class Meta:
        verbose_name = verbose_name_plural = u'Страница "Прайс"'


class WorkProgram(SingletonModel):
    """ РАБОЧАЯ  ПРОГРАММА цикла тематического усовершенствования
        «Фармацевтическое консультирование по безрецептурному отпуску» """
    title = models.TextField("Title", max_length=1000, default="Заголовок")
    keywords = models.CharField("Keywords", max_length=1000, default="Страница")
    description = models.TextField('Description', default="Описание страницы")
    text = HTMLField(default="Текст")

    class Meta:
        verbose_name = verbose_name_plural = u'РАБОЧАЯ  ПРОГРАММА цикла тематического усовершенствования"'
