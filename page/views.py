from django.core.mail import mail_admins
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.views.generic import TemplateView, FormView, ListView, DetailView

from .forms import CallbackForm, EntrantForm


def main(request):
    context = {'callback': CallbackForm(),
               'entrant': EntrantForm()}
    if request.POST.get('entrant'):
        form = EntrantForm(request.POST)
        if form.is_valid():
            f = form.save()
            params = (f.last_name, f.first_name, f.patronymic, f.phone, f.email, f.city)
            mail_admins(u'Новая заявка на поступление',
                        u'Имя: {} {} {}\nТелефон: {}\nEmail: {}\nГ.: {}'.format(*params))
            return JsonResponse({'status': 'ok'})
        html = render_to_string("entrant_form.html", {"entrant": form}, request=request)
        return JsonResponse({'status': 'error', 'html': html})
    elif request.POST.get('callback'):
        form = CallbackForm(request.POST)
        if form.is_valid():
            f = form.save()
            mail_admins(u'Обращение с сайта медицинской академии "Заказать звонок"',
                        u'Имя: {}\nТелефон: {}\nEmail: {}'.format(f.contact_name, f.phone, f.email))
            return JsonResponse({'status': 'ok'})
        html = render_to_string("callback_form.html", {"callback": form}, request=request)
        return JsonResponse({'status': 'error', 'html': html})
    return render(request, 'index.html', context=context)


class RequestCallView(FormView):
    form_class = CallbackForm
    template_name = "request_call.html"
    success_url = '/thank_you'

    def get_template_names(self):
        if not self.request.is_ajax():
            return "request_call_site.html"
        return self.template_name

    def form_valid(self, form):
        f = form.save()
        mail_admins(u'Обращение с сайта медицинской академии "Заказать звонок"',
                    u'Имя: {}\nТелефон: {}\nEmail: {}'.format(f.contact_name, f.phone, f.email))
        return JsonResponse({'error': 0, 'data': ''})

    def form_invalid(self, form):
        if not self.request.is_ajax():
            return self.render_to_response(self.get_context_data(form=form))
        csrf_token_value = self.request.COOKIES['csrftoken']
        data = render_to_string("request_call.html", self.get_context_data(form=form, csrf_token=csrf_token_value))
        return JsonResponse({'error': 1, 'data': data})