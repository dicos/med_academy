from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import RequestCall, Education, Entrant, SiteText, Consent, Lic, Price, WorkProgram


class RequestCallAdmin(admin.ModelAdmin):
    """ Заказать звонок """
    list_display = list_display_links = ('contact_name', 'phone', 'email', 'date_created')
    readonly_fields = ('date_created',)

admin.site.register(RequestCall, RequestCallAdmin)


admin.site.register(Education)


class EntrantAdmin(admin.ModelAdmin):
    """ Заявки на поступление в академию """
    list_display = list_display_links = ('last_name', 'first_name', 'patronymic', 'phone',
                                         'email', 'city', 'education', 'date_created',)
    readonly_fields = ('date_created',)

admin.site.register(Entrant, EntrantAdmin)


admin.site.register((SiteText, Consent, Lic, Price, WorkProgram), SingletonModelAdmin)