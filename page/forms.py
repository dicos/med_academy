#coding: utf8
from django import forms

from .models import RequestCall, Entrant


class CallbackForm(forms.ModelForm):
    class Meta:
        model = RequestCall
        fields = ['contact_name', 'phone', 'email']


class EntrantForm(forms.ModelForm):
    class Meta:
        model = Entrant
        exclude = ['date_created', ]