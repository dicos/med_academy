# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-28 15:06
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0004_auto_20161128_1433'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitetext',
            name='entrant_good',
            field=models.TextField(default='Заявка принята, в ближайшее время с вами свяжутся', verbose_name='Оставили заявку на поступление'),
        ),
        migrations.AlterField(
            model_name='entrant',
            name='phone',
            field=models.CharField(max_length=17, validators=[django.core.validators.RegexValidator(message='Номер телефона должен иметь такой формат: +7 (999) 999 99-99', regex='^\\+7 \\(\\d{3}\\) \\d{3}\\ \\d{2}-\\d{2}$')], verbose_name='Номер телефона*'),
        ),
    ]
