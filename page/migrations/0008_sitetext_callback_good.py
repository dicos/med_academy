# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-28 15:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('page', '0007_auto_20161128_1544'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitetext',
            name='callback_good',
            field=models.TextField(default='Заявка принята, в ближайшее время с вами свяжутся', verbose_name='Оставили заявку на обратный звонок'),
        ),
    ]
