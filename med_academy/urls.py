from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView

from page.views import main, RequestCallView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', main, name="main"),
    url(r'^request_call/$', RequestCallView.as_view(), name='request_call'),
    url(r'^consent/$', TemplateView.as_view(template_name='consent.html'), name='consent'),
    url(r'^lic/$', TemplateView.as_view(template_name='lic.html'), name='lic'),
    url(r'^price/$', TemplateView.as_view(template_name='price.html'), name='price'),
    url(r'^work-programm/$', TemplateView.as_view(template_name='work-programm.html'), name='price'),

    url(r'^tinymce/', include('tinymce.urls')),
]
