$(document).ready(function() {
    function init_forms() {
        $("#entrant-container #id_phone, #callback_form #id_phone").mask("+7 (999) 999 99-99");
    }
    init_forms();

    $("#entrant_form").submit(function(event) {
        event.preventDefault();
        var form = $(event.target);
        $.post(form.attr("action"), form.serialize(), function(data) {
            if (data.status == "error") {
                $("#entrant_form").replaceWith(data.html);
                init_forms();
            } else {
                $("#entrant_form,#entrant-container>div").hide();
                $("#entrant-good").show();
            }
        });
    })

    $("#callback_form").submit(function(event) {
        event.preventDefault();
        var form = $(event.target);
        $.post(form.attr("action"), form.serialize(), function(data) {
            if (data.status == "error") {
                $("#callback_form").replaceWith(data.html);
                init_forms();
            } else {
                $("#callback_form, .questions td").hide();
                $("#callback-good").show();
            }
        });
    })

    $('#request_call').click(function(){
        $("#callback_form, .questions td").show();
        $("#callback-good").hide();
        $("#callback_form").trigger('reset');
        $("#id_contact_name").focus();
    });
});
